pragma Singleton

import QtQuick 2.15
import Qt.labs.settings 1.0

QtObject {
    id: root

    property bool modulesRightAligned: true
}
