import QtQuick 2.15
import QtQuick.Controls 2.15

import "../images/"

Item
{
    id: main
    property int value: 0

    Image
    {
        anchors.centerIn: parent
        width:  Math.min(parent.width, parent.height)*0.8
        height: Math.min(parent.width, parent.height)*0.8

        source: (value==4) ? "../images/cp_100_pa.svg" : (value==3)
                           ? "../images/cp_16m_pa.svg" :(value==2)
                           ? "../images/cp_16m.svg" : (value==1)
                           ? "../images/cp_100.svg" : ""
        sourceSize.width:  width
        sourceSize.height: height
    }

    Connections
    {
        target: _uiCore
        function onSgSetUIParameter(nameParam, value)
        {
            if(nameParam===("type_dev"))
                main.value = value
        }
    }
}
